﻿using System;
//using UnityEngine;

namespace MapData
{
    [Serializable]
    public class MapData
    {
        public int width = 16;
        public int height = 16;
        public int length = 16;
        public float killPlane = 0;
        public float waterPlane = 1;
        public string name;
        public string desc;
        public byte[] mapBlockIds;
        public byte[] mapBlockDmg;
        public byte[] mapBlockForm;
        public byte[] mapBlockTint;

        public MapData(string n, int mw, int mh, int ml)
        {
            name = n;
            width = mw;
            height = mh;
            length = ml;
        }
        public String GetVersion()
        {
            return "1.2";
        }
    }
}