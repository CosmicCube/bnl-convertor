﻿using System;
using System.IO;
using Protocol;
using Json;
using System.Runtime.Serialization.Formatters.Binary;
using MapData;

namespace BNL_Map_Reader
{

    class Program
    {
        static byte[,] IDmap = new byte[,] {
                {0 ,0, 0},     //air
                {1 ,4, 1},     //dirt
                {2, 7, 1},     //sand
                {3, 14, 1},    //stone
                {4, 0, 0},     //lava ---
                {5, 3, 1},     //turf
                {6, 0, 0},     //tallgrass ---
                {7, 30, 0},    //bricks
                {8, 22, 0},    //planks
                {9, 18, 1},    //cement
                {10, 97, 0},   //metal
                {11, 20, 0},   //logs
                {12, 21, 1},   //leaves 
                {16, 43, 0},   //speedpads
                {17, 31, 0},   //crates
                {18, 44, 0},   //bouncepads
                {21, 33, 0},   //sandbagz
                {27, 15, 1},   //tiled-stone
                {29, 40, 0},   //glue
                {31, 41, 0},   //spikes
                {33, 34, 0},   //Forcegates
                {35, 15, 2},   //stone POST
                {37, 42, 0},   //beartraps
                {38, 16, 2},   //brick-stone POST
                {41, 22, 2},    //wood POST
                {44, 98, 0},   //IFgate
                {46, 12, 1},   //granite
                {51, 2, 0},    //bush -> MTurf
                {53, 17, 1},   //cementwhite -> clay
                {58, 31, 0},   //boxs -> crates
                {59, 99, 0},   //no-build
                {60, 9, 0},    //snow
                {61, 5, 0}     //ice
        };

        static void Main(string[] args)
        {
            string inFile = args[0];

            string json = MapStoreUtils.Decode(File.ReadAllBytes(inFile));
            JsonData jsonData = JsonParser.Parse(json);
            MapCustomData mcd = new MapCustomData();
            mcd.FromJsonData(jsonData);

            byte[] blockArray = ZLibHelper.UnZip(mcd.Map.BlocksData).ToArray();
            byte[] colourArray = ZLibHelper.UnZip(mcd.Map.ColorsData).ToArray();
            byte[] outBlocks = new byte[blockArray.Length / 4];
            byte[] outDamage = new byte[blockArray.Length / 4];
            byte[] outShape = new byte[blockArray.Length / 4];
            byte[] outColour = new byte[blockArray.Length / 4];

            for (int i = 0; i < blockArray.Length; i += 4)
            {
                byte ID = blockArray[i], DMG = blockArray[i + 1], FORM = blockArray[i + 2], TEAM = blockArray[i + 3], COL = colourArray[i / 4];
                byte id = ID, dmg = DMG, form = FORM, tint = COL;
                if (TEAM > 0) tint = TEAM;
                for (int itr = 0; itr < IDmap.Length / 3; itr += 1)
                {
                    if (IDmap[itr, 0] == ID)
                    {
                        id = IDmap[itr, 1];
                        if (FORM > 0 && IDmap[itr, 2] == 1) { dmg = 1; form = BLswap(FORM); }
                        else if (IDmap[itr, 2] == 2) { dmg = 2; form = 140; }
                    }
                }
                outBlocks[i / 4] = id;
                outDamage[i / 4] = dmg;
                outShape[i / 4] = form;
                outColour[i / 4] = tint;
            }

            MapData.MapData md = new MapData.MapData();
            md.width = mcd.Map.Size.z;
            md.height = mcd.Map.Size.y;
            md.length = mcd.Map.Size.x;
            md.killPlane = mcd.Map.Properties.KillPosition;
            md.waterPlane = mcd.Map.Properties.PlanePosition;
            md.name = mcd.Name;
            md.desc = mcd.Description;
            md.mapBlockIds = outBlocks;
            md.mapBlockDmg = outDamage;
            md.mapBlockForm = outShape;
            md.mapBlockTint = outColour;

            string outFile = Path.GetDirectoryName(inFile) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(inFile) + ".bytes";

            FileStream writerFileStream = new FileStream(outFile, FileMode.Create, FileAccess.Write);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(writerFileStream, md);
            //Console.Read(); //prevents program closing
        }
        static byte BLswap(byte form)
        {
            int p = 0; int by = 255 - form; bool[] crn = new bool[8], crn2 = new bool[8];
            for (int i = 128; i >= 1; i = i / 2)
            {
                if (by >= i) { crn[p] = true; by = by - i; }
                else crn[p] = false;
                p++;
            }
            crn2[0] = crn[3];
            crn2[1] = crn[0];
            crn2[2] = crn[5];
            crn2[3] = crn[2];
            crn2[4] = crn[6];
            crn2[5] = crn[1];
            crn2[6] = crn[7];
            crn2[7] = crn[4];
            by = 0; p = 0;
            for (int i = 128; i >= 1; i = i / 2)
            {
                if (crn2[p]) by += (byte)i;
                p++;
            }
            return (byte)by;
        }
    }
}
